﻿using OpenTK;
using OpenTK.Graphics.OpenGL;
using System;
using System.Drawing;

namespace Tutorial1
{
    /// <summary>
    /// Grab the Opentk library from nuget https://www.nuget.org/packages/OpenTK/
    /// Be sure to include the System.Drawing reference to the project
    /// The game window class includes a large amout of boiler plate code for windowing
    /// input and game loop functionality.
    /// </summary>
    public class Game : GameWindow
    {
        #region protected

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            // This tells the renderer what color to use when clearing the screen
            GL.ClearColor(Color.CornflowerBlue);
        }

        protected override void OnRenderFrame(FrameEventArgs e)
        {
            base.OnRenderFrame(e);

            //This command tells the renderer to clear various buffers to their preset values,
            //the color buffer utilizes the clear color as set in the OnLoad function
            GL.Clear(ClearBufferMask.ColorBufferBit);

            //creates a view matrix, like a camera
            var modelview = Matrix4.LookAt(Vector3.Zero, Vector3.UnitZ, Vector3.UnitY);

            //Sets the current matrix to the Modelview matrix
            GL.MatrixMode(MatrixMode.Modelview);

            //Loads the modelview matrix into the current matrix
            GL.LoadMatrix(ref modelview);

            //This function tells OpenGL that we want to start drawing vertices
            //there are various types of primitives we can draw from points to triangle strips and fans
            GL.Begin(PrimitiveType.Triangles);

            //This function tells opengl to set the color of the vertex draw state
            //all vertices drawn after this command will use the color provided
            GL.Color3(1.0f, 0.0f, 0.0f);

            //This command tells opengl to render a vertex at the provided position
            GL.Vertex3(-1.0f, -1.0f, 4.0f);

            GL.Color3(0.0f, 1.0f, 0.0f);

            GL.Vertex3(1.0f, -1.0f, 4.0f);

            GL.Color3(0.0f, 0.0f, 1.0f);

            GL.Vertex3(0.0f, 1.0f, 4.0f);

            //This function tells opengl we are done drawing vertices
            GL.End();

            //To prevent flicking that occurs when drawing direct to the screen we utilize a double buffered approach
            //By default our drawing functions draw to the back buffer. This command tells opengl to swap the backbuffer with the front buffer.
            SwapBuffers();
        }

        protected override void OnResize(EventArgs e)
        {
            base.OnResize(e);

            //The opengl viewport defines the part of the render context we will actually draw to with our render functions
            GL.Viewport(ClientRectangle.X, ClientRectangle.Y, ClientRectangle.Width, ClientRectangle.Height);

            //Creates a perspective matrix and stores it 90 degree FOV, aspect ratio equals the aspect ration of the drawing context
            //The near and far parameters are your frustrum values, nothing less that the near distance or farther than
            //the far distance will be rendered
            //https://randomcodingprojects.files.wordpress.com/2012/08/fp1-frustrum1.png?w=640
            var projection = Matrix4.CreatePerspectiveFieldOfView((float)Math.PI / 4, Width / (float)Height, 1.0f, 64.0f);

            //Sets the current matrix to the Projection matrix
            GL.MatrixMode(MatrixMode.Projection);

            //Loads the projection matrix intot he current matrix
            GL.LoadMatrix(ref projection);
        }

        #endregion protected
    }
}
﻿using OpenTK;
using OpenTK.Graphics.OpenGL;
using System;
using System.Drawing;
using System.IO;
using System.ComponentModel;

namespace Tutorial2
{
    /// <summary>
    /// Grab the Opentk library from nuget https://www.nuget.org/packages/OpenTK/
    /// Be sure to include the System.Drawing reference to the project
    /// The game window class includes a large amout of boiler plate code for windowing
    /// input and game loop functionality.
    /// </summary>
    public class Game : GameWindow
    {
        private int _shaderProgramId;
        private int _vertexArrayObject;
        private int _vboPosition;
        private int _vboColor;
        private int _vboIndice;
        private Matrix4 _projection;

        #region private

        private void CompileShaderProgram()
        {
            //generate shader program id
            _shaderProgramId = GL.CreateProgram();

            //generate ids for the shaders
            var vertexShaderId = GL.CreateShader(ShaderType.VertexShader);
            var fragmentShaderId = GL.CreateShader(ShaderType.FragmentShader);

            //load the shader source code from files
            var vertexShaderSource = File.ReadAllText(AppDomain.CurrentDomain.BaseDirectory + "Assets/Shaders/SimpleShader.vert");
            var fragmentShaderSource = File.ReadAllText(AppDomain.CurrentDomain.BaseDirectory + "Assets/Shaders/SimpleShader.frag");

            //load the vertex shader source into opengl
            GL.ShaderSource(vertexShaderId, vertexShaderSource);
            GL.ShaderSource(fragmentShaderId, fragmentShaderSource);

            //compile the vertex shader
            GL.CompileShader(vertexShaderId);
            GL.CompileShader(fragmentShaderId);

            //attach shaders to program
            GL.AttachShader(_shaderProgramId, vertexShaderId);
            GL.AttachShader(_shaderProgramId, fragmentShaderId);

            //link program
            GL.LinkProgram(_shaderProgramId);

            //detach shaders
            GL.DetachShader(_shaderProgramId, vertexShaderId);
            GL.DetachShader(_shaderProgramId, fragmentShaderId);

            //cleanup
            GL.DeleteShader(vertexShaderId);
            GL.DeleteShader(fragmentShaderId);
        }

        private void LoadGeometry()
        {
            //3 verts for the triangle
            var vertexData = new Vector3[] {
                new Vector3(-1.0f, -1.0f, 0.0f),
                new Vector3(1.0f, -1.0f, 0.0f),
                new Vector3(0.0f, 1.0f, 0.0f)
            };

            //each vert is a different color. the color of the pixels will be
            //interpolated betweeen the points
            var colorData = new Vector3[] {
                new Vector3( 1f, 0f, 0f),
                new Vector3( 0f, 0f, 1f),
                new Vector3( 0f, 1f, 0f)
            };

            //indices, signify which vertex to use, this saves data for reused vertices
            var indiceData = new uint[] {
                0, 1, 2
            };

            //generate ids
            GL.GenVertexArrays(1, out _vertexArrayObject);
            GL.GenBuffers(1, out _vboPosition);
            GL.GenBuffers(1, out _vboColor);
            GL.GenBuffers(1, out _vboIndice);

            //bind the vertex array object, all of the next calls will effect this vertex array object
            GL.BindVertexArray(_vertexArrayObject);

            //tell opengl to bind the position buffer, then set the data for that buffer
            GL.BindBuffer(BufferTarget.ArrayBuffer, _vboPosition);
            GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(vertexData.Length * Vector3.SizeInBytes), vertexData, BufferUsageHint.StaticDraw);

            //enable the position attribute, and tell opengl where the data is
            GL.EnableVertexAttribArray(GL.GetAttribLocation(_shaderProgramId, "vPosition"));
            GL.VertexAttribPointer(GL.GetAttribLocation(_shaderProgramId, "vPosition"), 3, VertexAttribPointerType.Float, true, 0, 0);

            //tell opengl to bind the color buffer, then set the data for that buffer
            GL.BindBuffer(BufferTarget.ArrayBuffer, _vboColor);
            GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(colorData.Length * Vector3.SizeInBytes), colorData, BufferUsageHint.StaticDraw);

            //enable the position attribute, and tell opengl where the data is
            GL.EnableVertexAttribArray(GL.GetAttribLocation(_shaderProgramId, "vColor"));
            GL.VertexAttribPointer(GL.GetAttribLocation(_shaderProgramId, "vColor"), 3, VertexAttribPointerType.Float, false, 0, 0);

            //load indices into buffer and enable in vertex array object
            GL.BindBuffer(BufferTarget.ElementArrayBuffer, _vboIndice);
            GL.BufferData(BufferTarget.ElementArrayBuffer, (IntPtr)(indiceData.Length * sizeof(uint)), indiceData, BufferUsageHint.StaticDraw);

            //clear the bound buffer, so nothign funny happens
            GL.BindVertexArray(0);
        }

        #endregion private

        #region protected

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            // This tells the renderer what color to use when clearing the screen
            GL.ClearColor(Color.CornflowerBlue);

            //Creates a perspective matrix and stores it 90 degree FOV, aspect ratio equals the aspect ration of the drawing context
            //The near and far parameters are your frustrum values, nothing less that the near distance or farther than
            //the far distance will be rendered
            //https://randomcodingprojects.files.wordpress.com/2012/08/fp1-frustrum1.png?w=640
            _projection = Matrix4.CreatePerspectiveFieldOfView((float)Math.PI / 4, Width / (float)Height, 0.1f, 64.0f);

            CompileShaderProgram();

            LoadGeometry();
        }

        protected override void OnRenderFrame(FrameEventArgs e)
        {
            base.OnRenderFrame(e);

            //This command tells the renderer to clear various buffers to their preset values,
            //the color buffer utilizes the clear color as set in the OnLoad function
            GL.Clear(ClearBufferMask.ColorBufferBit);

            //this tells the graphics card which shader program to run when rendering
            GL.UseProgram(_shaderProgramId);

            //bind our vertex array object
            GL.BindVertexArray(_vertexArrayObject);

            //create camera view transform
            var view = Matrix4.LookAt(Vector3.Zero, Vector3.UnitZ, Vector3.UnitY);

            //multiply with projection
            var mvp = Matrix4.CreateTranslation(0, 0, 4) * view * _projection;

            //set our model view transform
            GL.UniformMatrix4(GL.GetUniformLocation(_shaderProgramId, "modelview"), false, ref mvp);

            //draw the elements that we have setup, we only have 3 elements to draw
            GL.DrawElements(BeginMode.Triangles, 3, DrawElementsType.UnsignedInt, 0);

            //disable the vertex array object
            GL.BindVertexArray(0);

            //To prevent flicking that occurs when drawing direct to the screen we utilize a double buffered approach
            //By default our drawing functions draw to the back buffer. This command tells opengl to swap the backbuffer with the front buffer.
            SwapBuffers();
        }

        protected override void OnResize(EventArgs e)
        {
            base.OnResize(e);

            //The opengl viewport defines the part of the render context we will actually draw to with our render functions
            GL.Viewport(ClientRectangle.X, ClientRectangle.Y, ClientRectangle.Width, ClientRectangle.Height);
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            base.OnClosing(e);

            //cleanup
            GL.DeleteProgram(_shaderProgramId);
        }

        #endregion protected
    }
}
#version 330

uniform mat4 modelview;

in vec3 vPosition;
in vec3 vColor;

out vec4 color;

void main()
{
  gl_Position = modelview * vec4(vPosition, 1.0);
  
  color = vec4(vColor, 1.0);
}

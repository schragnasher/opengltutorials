﻿using OpenTK.Graphics.OpenGL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Tutorial3.Models
{
    public class Mesh
    {
        private uint _vertexArrayObject;
        private uint _vertexBufferObject;
        private uint _indexBufferObject;
        private Vertex[] _vertices;
        private uint[] _indices;

        internal Mesh(Vertex[] vertices, uint[] indices)
        {
            _vertices = vertices;
            _indices = indices;

            Initialize();
        }

        #region public

        public int MaterialIndex { get; internal set; }

        public void Render()
        {
            GL.BindVertexArray(_vertexArrayObject);

            GL.DrawElements(PrimitiveType.Triangles, _indices.Length, DrawElementsType.UnsignedInt, 0);

            GL.BindVertexArray(0);
        }

        #endregion public

        #region private

        internal void Initialize()
        {
            //create buffers for mesh
            GL.CreateVertexArrays(1, out _vertexArrayObject);
            GL.CreateBuffers(1, out _vertexBufferObject);
            GL.CreateBuffers(1, out _indexBufferObject);

            //bind vertex array
            GL.BindVertexArray(_vertexArrayObject);

            //load vertices
            GL.BindBuffer(BufferTarget.ArrayBuffer, _vertexBufferObject);
            GL.BufferData(BufferTarget.ArrayBuffer, _vertices.Length * Marshal.SizeOf(typeof(Vertex)), _vertices, BufferUsageHint.StaticDraw);

            //load indices
            GL.BindBuffer(BufferTarget.ElementArrayBuffer, _indexBufferObject);
            GL.BufferData(BufferTarget.ElementArrayBuffer, _indices.Length * sizeof(uint), _indices, BufferUsageHint.StaticDraw);

            //setup vertex position pointer
            GL.EnableVertexAttribArray(0);
            GL.VertexAttribPointer(0, 3, VertexAttribPointerType.Float, false, Marshal.SizeOf(typeof(Vertex)), 0);

            //setup vertex color pointer
            GL.EnableVertexAttribArray(1);
            GL.VertexAttribPointer(1, 3, VertexAttribPointerType.Float, false, Marshal.SizeOf(typeof(Vertex)), Marshal.OffsetOf(typeof(Vertex), "Color"));

            GL.EnableVertexAttribArray(0);
        }

        #endregion private
    }
}
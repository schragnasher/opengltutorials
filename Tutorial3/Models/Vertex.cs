﻿using OpenTK;
using System.Runtime.InteropServices;

namespace Tutorial3.Models
{
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct Vertex
    {
        public Vector3 Position;
        public Vector3 Color;
    }
}
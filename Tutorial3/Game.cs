﻿using OpenTK;
using OpenTK.Graphics.OpenGL;
using System;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using Tutorial3.Models;
using Tutorial3.Utility;

namespace Tutorial3
{
    /// <summary>
    /// Grab the Opentk library from nuget https://www.nuget.org/packages/OpenTK/
    /// Be sure to include the System.Drawing reference to the project
    /// The game window class includes a large amout of boiler plate code for windowing
    /// input and game loop functionality.
    /// </summary>
    public class Game : GameWindow
    {
        private int _shaderProgramId;
        private Matrix4 _transform;
        private Mesh _mesh;
        private float _time;

        #region private

        private void CompileShaderProgram()
        {
            //generate shader program id
            _shaderProgramId = GL.CreateProgram();

            //generate ids for the shaders
            var vertexShaderId = GL.CreateShader(ShaderType.VertexShader);
            var fragmentShaderId = GL.CreateShader(ShaderType.FragmentShader);

            //load the shader source code from files
            var vertexShaderSource = File.ReadAllText(AppDomain.CurrentDomain.BaseDirectory + "Assets/Shaders/SimpleShader.vert");
            var fragmentShaderSource = File.ReadAllText(AppDomain.CurrentDomain.BaseDirectory + "Assets/Shaders/SimpleShader.frag");

            //load the vertex shader source into opengl
            GL.ShaderSource(vertexShaderId, vertexShaderSource);
            GL.ShaderSource(fragmentShaderId, fragmentShaderSource);

            //compile the vertex shader
            GL.CompileShader(vertexShaderId);
            GL.CompileShader(fragmentShaderId);

            //attach shaders to program
            GL.AttachShader(_shaderProgramId, vertexShaderId);
            GL.AttachShader(_shaderProgramId, fragmentShaderId);

            //link program
            GL.LinkProgram(_shaderProgramId);

            //detach shaders
            GL.DetachShader(_shaderProgramId, vertexShaderId);
            GL.DetachShader(_shaderProgramId, fragmentShaderId);

            //cleanup
            GL.DeleteShader(vertexShaderId);
            GL.DeleteShader(fragmentShaderId);
        }

        private void LoadGeometry()
        {
            var meshBuilder = new MeshBuilder();

            meshBuilder.AddVertex(new Vertex() { Position = new Vector3(-5.0f, -5.0f, -5.0f), Color = new Vector3(1, 0, 0) });
            meshBuilder.AddVertex(new Vertex() { Position = new Vector3(5.0f, -5.0f, -5.0f), Color = new Vector3(0, 1, 0) });
            meshBuilder.AddVertex(new Vertex() { Position = new Vector3(5.0f, 5.0f, -5.0f), Color = new Vector3(0, 0, 1) });
            meshBuilder.AddVertex(new Vertex() { Position = new Vector3(-5.0f, 5.0f, -5.0f), Color = new Vector3(1, 1, 0) });

            meshBuilder.AddVertex(new Vertex() { Position = new Vector3(-5.0f, -5.0f, 5.0f), Color = new Vector3(1, 0, 0) });
            meshBuilder.AddVertex(new Vertex() { Position = new Vector3(5.0f, -5.0f, 5.0f), Color = new Vector3(0, 1, 0) });
            meshBuilder.AddVertex(new Vertex() { Position = new Vector3(5.0f, 5.0f, 5.0f), Color = new Vector3(0, 0, 1) });
            meshBuilder.AddVertex(new Vertex() { Position = new Vector3(-5.0f, 5.0f, 5.0f), Color = new Vector3(1, 1, 0) });

            meshBuilder.AddTriangle(0, 1, 2);
            meshBuilder.AddTriangle(0, 2, 3);
            meshBuilder.AddTriangle(1, 2, 6);
            meshBuilder.AddTriangle(1, 6, 5);
            //meshBuilder.AddTriangle(5, 4, 7);
            //meshBuilder.AddTriangle(5, 7, 6);
            //meshBuilder.AddTriangle(4, 0, 3);
            //meshBuilder.AddTriangle(4, 3, 7);
            //meshBuilder.AddTriangle(3, 2, 6);
            //meshBuilder.AddTriangle(3, 6, 7);
            //meshBuilder.AddTriangle(0, 1, 5);
            //meshBuilder.AddTriangle(0, 5, 4);

            _mesh = meshBuilder.CreateMesh();
        }

        #endregion private

        #region protected

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            // This tells the renderer what color to use when clearing the screen
            GL.ClearColor(Color.CornflowerBlue);

            GL.Enable(EnableCap.CullFace);
            GL.Enable(EnableCap.DepthTest);

            CompileShaderProgram();

            LoadGeometry();
        }

        protected override void OnUpdateFrame(FrameEventArgs e)
        {
            base.OnUpdateFrame(e);

            //Creates a perspective matrix and stores it 90 degree FOV, aspect ratio equals the aspect ration of the drawing context
            //The near and far parameters are your frustrum values, nothing less that the near distance or farther than
            //the far distance will be rendered
            //https://randomcodingprojects.files.wordpress.com/2012/08/fp1-frustrum1.png?w=640
            var projection = Matrix4.CreatePerspectiveFieldOfView((float)Math.PI / 4, Width / (float)Height, 0.1f, 64.0f);

            _time += (float)e.Time;

            //create the transform of our triangle, 30 units away for our view with our given projection
            _transform = Matrix4.CreateRotationX(_time) * Matrix4.CreateRotationY(_time) * Matrix4.CreateTranslation(0, 0, -30) * projection;
        }

        protected override void OnRenderFrame(FrameEventArgs e)
        {
            base.OnRenderFrame(e);

            //This command tells the renderer to clear various buffers to their preset values,
            //the color buffer utilizes the clear color as set in the OnLoad function
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);

            //this tells the graphics card which shader program to run when rendering
            GL.UseProgram(_shaderProgramId);

            //set our model view transform
            GL.UniformMatrix4(GL.GetUniformLocation(_shaderProgramId, "modelview"), false, ref _transform);

            _mesh.Render();

            //To prevent flicking that occurs when drawing direct to the screen we utilize a double buffered approach
            //By default our drawing functions draw to the back buffer. This command tells opengl to swap the backbuffer with the front buffer.
            SwapBuffers();
        }

        protected override void OnResize(EventArgs e)
        {
            base.OnResize(e);

            //The opengl viewport defines the part of the render context we will actually draw to with our render functions
            GL.Viewport(ClientRectangle.X, ClientRectangle.Y, ClientRectangle.Width, ClientRectangle.Height);
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            base.OnClosing(e);

            //cleanup
            GL.DeleteProgram(_shaderProgramId);
        }

        #endregion protected
    }
}
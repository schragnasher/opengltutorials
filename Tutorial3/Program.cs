﻿namespace Tutorial3
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            using (Game game = new Game())
            {
                //Start the OpenTK game window with an update rate
                //of 30 updates a second, with unconstrained render rate
                game.Run(30.0);
            }
        }
    }
}